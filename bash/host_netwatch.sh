#!/bin/bash

#	this script will ping a host, will display the host state "UP" or
#	"DOWN" an it will write to a file named host_netwatch.log the state of
#	the host the date and time when host state was checked

#	script to make use of a function, a boolean variable state,
#	conditionals, writing in append mode to a .log file
#		make script executable:
# use: chmod +x host_netwatch before running the script
# example: ./host_netwatch google.com

HOST=$1
GREEN='\033[0;32m'        # Green
NC='\033[0m' 							# No Color
RED='\033[0;31m'					# Red

function DATE (){
					Year=`date +%Y`
					Month=`date +%m`
					Day=`date +%d`
					Hour=`date +%H`
					Minute=`date +%M`
					Second=`date +%S`
					echo `date`
#					echo "Current Date is: $Day-$Month-$Year"
#					echo "Current Time is: $Hour:$Minute:$Second"
}

#command to check if host state
ping -c 1 $HOST

#if exit code of the command ping -c 3 $HOST is equal with 0 ...
if [ "$?" -eq "0" ]; then
				printf "${GREEN} $HOST is UP \n...log written in host_netwatch.log${NC}\n"

				echo " The host: $HOST was UP at: $(DATE) " >> /home/dell/scripting/host_netwatch.log

else
				printf "${RED} $HOST is DOWN \n...log written in host_netwatch.log${NC}\n"

				echo " The host:  $HOST was DOWN at: $(DATE) " >>	/home/dell/scripting/host_netwatch.log
fi

