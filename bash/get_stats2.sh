#!/bin/bash

: ' un script care primeste un parametru care poate fi "load", "mem", "disk" si
sa faca urmatoarele: daca parametrul este "load", vreau sa afisati load-ul
pentru ultimele 5 minute, daca parametrul este "mem" vreau sa afisati memoria
free in MB iar daca parametrul este "disk" vreau sa afisati cat spatiu liber
este pe root "/" '

: " awk 'NR == 1 {print $2}' (from line 1 print column 2) 
 NR - It gives the total number of records processed.
 FNR - It gives the total number of records for each input file.
- cat /proc/loadavg The first three fields are 1, 5-minute, and 15-minute load averages
- uptime can be used also to get the last 5 min cpu load "

DISK=$(df -h / | awk 'NR == 2 {print $4}')
LOAD=$(cat /proc/loadavg | awk 'NR == 1 {print $2}')
MEM=$(free -m | awk 'NR == 2 {print $4}')

#DISK=$(df -h / | grep sda | tr -s ' '| cut -d ' ' -f4)
#LOAD=$(cat /proc/loadavg | cut -d ' ' -f 2)
#MEM=$(free -m | grep Mem | tr -s ' '| cut -d ' ' -f4)

#tr utility translates single characters into other characters(tr -s ' '=squeeze repeats)
# "$@" expands into a list of separate parameters

for PARAM in "$@"; do
	if [ $PARAM == disk ]; then
        echo "the root partition free space is $DISK"
elif [ $PARAM == mem ]; then
        echo "the FREE RAM memory is: $MEM MB"
elif [ $PARAM == load ]; then
        echo " the last 5 min cpu load is : $LOAD"
else
				echo " unknown parameter, please enter only parameters like: disk mem load "
	fi
done

