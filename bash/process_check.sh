#!/bin/bash


PROCESS=$1

GREEN='\033[0;32m'        # Green
NC='\033[0m'              # No Color
RED='\033[0;31m'          # Red
BLUE='\033[0;34m'         # Blue

if pgrep -x $PROCESS >/dev/null; then
    printf "${GREEN} $PROCESS ${NC} is running\n"
		
else
    	printf "${RED} $PROCESS ${NC} is stopped\n"
			printf "starting ${BLUE} $PROCESS ${NC}\n"
			systemctl start nginx 
fi
