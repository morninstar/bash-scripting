#!/bin/bash

#Note: pgrep -x name or pidof name still reports that name is running even when it is defunct (zombie); so to get alive-and-running process, we need to use ps and grep

# 2> redirects stderr to an (unspecified) file
# &1 redirects stderr to stdout.
# ps -o ( user defined format)
# ps -o comm= -C process (it will display only the process name)
# grep -x ( will grep the exact string name)

PROCESS=$1

GREEN='\033[0;32m'        # Green
NC='\033[0m'              # No Color
RED='\033[0;31m'          # Red
BLUE='\033[0;34m'         # Blue


function isRunning() {
    ps -o comm= -C $PROCESS 2>/dev/null | grep -x $PROCESS >/dev/null 2>&1
} 

if ! isRunning $PROCESS; then
	printf "${RED} $PROCESS ${NC} is stopped\n"
	printf "starting ${BLUE} $PROCESS ${NC}\n"
	service nginx start
else
	printf "${GREEN} $PROCESS ${NC} is running\n"
fi
